import cn from 'classnames';
import styles from './Button.module.css';


function Button(props: {className: string, children: string, onClick: () => void}) {
  return (<button className={cn(styles.btn, `${props.className}`)} onClick={props.onClick}>{props.children}</button>);
}

export default Button;