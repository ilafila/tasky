import { useCallback, useContext } from "react";
import { GoogleLogout } from 'react-google-login';
import { StoreContext } from "../store";

function GoogleLogOutBtn() {

  const globalStore = useContext(StoreContext);

  const handleLogOut = useCallback(() => {
    globalStore.services.googleLogOutService.logOut();
  }, [globalStore.services.googleLogOutService]);

  return (
    <GoogleLogout
      clientId="284037732309-13d15q6lfh4nelel9mcf0ap7p6b6jlup.apps.googleusercontent.com"
      buttonText="Logout"
      onLogoutSuccess={handleLogOut}
      onFailure={fail}
    />
  );
}

export default GoogleLogOutBtn;