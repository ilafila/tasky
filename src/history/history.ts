import { createBrowserHistory } from 'history';


const newHistory = createBrowserHistory({ basename: '/' });

export default newHistory;
